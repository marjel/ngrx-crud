import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {HomeComponent} from './ui/home/home/home.component';

export const routes: Routes = [
  {
    path: 'home',
    pathMatch: 'full',
    component: HomeComponent
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/parametrization'
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {useHash: false, enableTracing: false})
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
