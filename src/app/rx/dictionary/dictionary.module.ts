import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import {DictionaryReducer} from './store/dictionary.reducer';
import {DictionaryEffects} from './store/dictionary.effect';
import {EffectsModule} from '@ngrx/effects';
import {CommonModule} from '@angular/common';
import {DictionaryEntity} from './model/dictionary.entity';


@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature(DictionaryEntity.type, DictionaryReducer.Reducer()),
    EffectsModule.forFeature([DictionaryEffects])
  ],
  providers: [
  ]
})
export class DictionaryModule {

}
