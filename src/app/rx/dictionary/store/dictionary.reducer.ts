import {DictionaryEntity} from '../model/dictionary.entity';
import {CrudReducer} from '../../../ngrx-crud/rx/crud.reducer';
import {DictionaryState} from '../model/dictionary.state';
import {CrudState} from '../../../ngrx-crud/model/crud.state';
import {CrudAction} from '../../../ngrx-crud/rx/crud.action';

export class DictionaryReducer extends CrudReducer<DictionaryEntity> {
  
  static Reducer(): Function {
    return DictionaryReducer.prototype.reducer;
  }
  
  reducer(state: DictionaryState = DictionaryState.getDefault(),
          action: CrudAction<DictionaryEntity>): CrudState<DictionaryEntity> {
    return super.reducer(state, action);
  }
}
