import {DictionaryEntity} from '../model/dictionary.entity';
import {CrudEffect} from '../../../ngrx-crud/rx/crud.effect';
import {Actions, Effect} from '@ngrx/effects';
import {CrudActionFactory} from '../../../ngrx-crud/rx/crud-action-factory';
import {CrudService} from '../../../ngrx-crud/persistence/crud.service';
import {Inject, Injectable} from '@angular/core';
import {CrudAction} from '../../../ngrx-crud/rx/crud.action';
import {Observable} from 'rxjs/Observable';

Injectable()
export class DictionaryEffects extends CrudEffect<DictionaryEntity> {
  protected payloadType: string = DictionaryEntity.type;

  /*
   * create
   * */
  @Effect()
  createDictionary$: Observable<CrudAction<DictionaryEntity>> = this.create();

  /*
   * read
   * */
  @Effect()
  readDictionary$: Observable<CrudAction<DictionaryEntity>> = this.read();

  /*
   * update
   * */
  @Effect()
  updateDictionary$: Observable<CrudAction<DictionaryEntity>> = this.update();

  /*
   * dispose
   * */
  @Effect()
  disposeDictionary$: Observable<CrudAction<DictionaryEntity>> = this.dispose();
  constructor(@Inject(Actions) actions$: Actions,
                @Inject(CrudActionFactory) actionFactory: CrudActionFactory,
                @Inject(CrudService) service: CrudService) {
    super(actions$, actionFactory, service);
    }
}
