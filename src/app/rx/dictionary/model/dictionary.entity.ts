import {CrudEntity} from '../../../ngrx-crud/model/crud.entity';

export class DictionaryEntity extends CrudEntity {

  private _fieldName: string;
  private _tableName: string;
  private _value1: string;
  private _value2: string;
  private _market: string;
  private _deposit: string;
  private _description: string;

  static get type(): string {
    return 'dictionary';
  }
  getType(): string {
    return DictionaryEntity.type;
  }
  public get fieldName(): string {
    return this._fieldName;
  }

  public set fieldName(value: string) {
    this._fieldName = value;
  }

  public get tableName(): string {
    return this._tableName;
  }

  public set tableName(value: string) {
    this._tableName = value;
  }

  public get value1(): string {
    return this._value1;
  }

  public set value1(value: string) {
    this._value1 = value;
  }

  public get value2(): string {
    return this._value2;
  }

  public set value2(value: string) {
    this._value2 = value;
  }

  public get market(): string {
    return this._market;
  }

  public set market(value: string) {
    this._market = value;
  }

  public get deposit(): string {
    return this._deposit;
  }

  public set deposit(value: string) {
    this._deposit = value;
  }

  public get description(): string {
    return this._description;
  }

  public set description(value: string) {
    this._description = value;
  }
  constructor (id: string, dateCreate: Date, dateUpdate: Date, userCreate: string, userUpdate: string,
              fieldName: string,
              tableName: string,
              value1: string, value2: string,
              market: string, deposit: string,
              description: string) {
    super(id, dateCreate, dateUpdate, userCreate, userUpdate);
    this._fieldName = fieldName;
    this._tableName = tableName;
    this._value1 = value1;
    this._value2 = value2;
    this._market = market;
    this._deposit = deposit;
    this._description = description;
  }
}
