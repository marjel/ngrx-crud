import {CrudState} from '../../../ngrx-crud/model/crud.state';
import {DictionaryEntity} from './dictionary.entity';

export class DictionaryState extends CrudState<DictionaryEntity> {
  
  static getDefault(): CrudState<DictionaryEntity> {
    const state: DictionaryState = new DictionaryState();
    state.data = new Array<DictionaryEntity>();
    state.current = null;
    return state;
  }
}
