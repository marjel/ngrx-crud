import {BookEntity} from '../model/book.entity';
import {CrudEffect} from '../../../ngrx-crud/rx/crud.effect';
import {Actions, Effect} from '@ngrx/effects';
import {CrudActionFactory} from '../../../ngrx-crud/rx/crud-action-factory';
import {CrudService} from '../../../ngrx-crud/persistence/crud.service';
import {Inject, Injectable} from '@angular/core';
import {CrudAction} from '../../../ngrx-crud/rx/crud.action';
import {Observable} from 'rxjs/Observable';

Injectable()
export class BookEffects extends CrudEffect<BookEntity> {

  protected payloadType: string = BookEntity.type;

  /*
   * create
   * */
  @Effect()
  createBook$: Observable<CrudAction<BookEntity>> = this.create();

  /*
   * read
   * */
  @Effect()
  readBook$: Observable<CrudAction<BookEntity>> = this.read();

  /*
   * update
   * */
  @Effect()
  updateBook$: Observable<CrudAction<BookEntity>> = this.update();

  /*
   * dispose
   * */
  @Effect()
  disposeBook$: Observable<CrudAction<BookEntity>> = this.dispose();

  constructor(@Inject(Actions) actions$: Actions,
                @Inject(CrudActionFactory) actionFactory: CrudActionFactory,
                @Inject(CrudService) service: CrudService) {
    super(actions$, actionFactory, service);
  }
}
