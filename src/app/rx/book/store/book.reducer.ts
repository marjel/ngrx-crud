import {BookEntity} from '../model/book.entity';
import {CrudReducer} from '../../../ngrx-crud/rx/crud.reducer';
import {CrudState} from '../../../ngrx-crud/model/crud.state';
import {BookState} from '../model/book.state';
import {CrudAction} from '../../../ngrx-crud/rx/crud.action';

export class BookReducer extends CrudReducer<BookEntity> {
  static Reducer(): Function {
    return BookReducer.prototype.reducer;
  }

  reducer(state: BookState = BookState.getDefault(),
          action: CrudAction<BookEntity>): CrudState<BookEntity> {
    return super.reducer(state, action);
  }
}
