import {CrudState} from '../../../ngrx-crud/model/crud.state';
import {BookEntity} from './book.entity';

export class BookState extends CrudState<BookEntity> {
  static getDefault(): BookState {
    const state: BookState = new BookState();
    state.data = new Array<BookEntity>();
    state.current = null;
    return state;
  }
}
