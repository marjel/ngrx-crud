import {CrudEntity} from '../../../ngrx-crud/model/crud.entity';

export class BookEntity extends CrudEntity {

  private _title: string;
  private _author: string;
  private _year: string;

  static get type(): string {
    return 'book';
  }

  getType(): string {
    return BookEntity.type;
  }

  get title(): string {
    return this._title;
  }

  set title(value: string) {
    this._title = value;
  }

  get author(): string {
    return this._author;
  }

  set author(value: string) {
    this._author = value;
  }

  get year(): string {
    return this._year;
  }

  set year(value: string) {
    this._year = value;
  }


  constructor(id: string, dateCreate: Date, dateUpdate: Date, userCreate: string, userUpdate: string,
              title: string, author: string, year: string) {
    super(id, dateCreate, dateUpdate, userCreate, userUpdate);
    this._title = title;
    this._author = author;
    this._year = year;
  }
}
