import {NgModule} from '@angular/core';
import {StoreModule} from '@ngrx/store';
import {BookReducer} from './store/book.reducer';
import {BookEffects} from './store/book.effect';
import {EffectsModule} from '@ngrx/effects';
import {CommonModule} from '@angular/common';
import {BookEntity} from './model/book.entity';


@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature(BookEntity.type, BookReducer.Reducer()),
    EffectsModule.forFeature([BookEffects])
  ],
  providers: [
  ]
})
export class BookModule {

}
