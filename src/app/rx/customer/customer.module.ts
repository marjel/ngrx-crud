import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import {CustomerReducer} from './store/customer.reducer';
import {CustomerEffects} from './store/customer.effect';
import {EffectsModule} from '@ngrx/effects';
import {CommonModule} from '@angular/common';
import {CustomerEntity} from './model/customer.entity';


@NgModule({
  imports: [
    CommonModule,
    StoreModule.forFeature(CustomerEntity.type, CustomerReducer.Reducer()),
    EffectsModule.forFeature([CustomerEffects])
  ],
  providers: [
  ]
})
export class CustomerModule {

}
