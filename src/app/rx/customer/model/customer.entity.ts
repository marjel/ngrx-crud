import {CrudEntity} from '../../../ngrx-crud/model/crud.entity';

export class CustomerEntity extends CrudEntity {

  private _language: string;
  private _nbp1: number;
  private _nbp2: number;
  private _deposit: string;
  private _tax1: number;
  private _tax2: number;

  static get type(): string {
    return 'customer';
  }
  getType(): string {
    return CustomerEntity.type;
  }

  get language(): string {
    return this._language;
  }

  set language(value: string) {
    this._language = value;
  }

  get nbp1(): number {
    return this._nbp1;
  }

  set nbp1(value: number) {
    this._nbp1 = value;
  }

  get nbp2(): number {
    return this._nbp2;
  }

  set nbp2(value: number) {
    this._nbp2 = value;
  }

  get deposit(): string {
    return this._deposit;
  }

  set deposit(value: string) {
    this._deposit = value;
  }

  get tax1(): number {
    return this._tax1;
  }

  set tax1(value: number) {
    this._tax1 = value;
  }

  get tax2(): number {
    return this._tax2;
  }

  set tax2(value: number) {
    this._tax2 = value;
  }

  constructor(id: string, dateCreate: Date, dateUpdate: Date, userCreate: string, userUpdate: string,
              language: string, nbp1: number, nbp2: number,
              deposit: string, tax1: number, tax2: number) {
    super(id, dateCreate, dateUpdate, userCreate, userUpdate);
    this._language = language;
    this._nbp1 = nbp1;
    this._nbp2 = nbp2;
    this._deposit = deposit;
    this._tax1 = tax1;
    this._tax2 = tax2;
  }
}
