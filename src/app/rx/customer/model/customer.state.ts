import {CrudState} from '../../../ngrx-crud/model/crud.state';
import {CustomerEntity} from './customer.entity';

export class CustomerState extends CrudState<CustomerEntity> {

  static getDefault(): CrudState<CustomerEntity> {
    const state: CustomerState = new CustomerState();
    state.data = new Array<CustomerEntity>();
    state.current = null;
    return state;
  }
}
