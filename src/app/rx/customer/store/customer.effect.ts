import {CustomerEntity} from '../model/customer.entity';
import {CrudEffect} from '../../../ngrx-crud/rx/crud.effect';
import {Actions, Effect} from '@ngrx/effects';
import {CrudActionFactory} from '../../../ngrx-crud/rx/crud-action-factory';
import {CrudService} from '../../../ngrx-crud/persistence/crud.service';
import {Inject, Injectable} from '@angular/core';
import {CrudAction} from '../../../ngrx-crud/rx/crud.action';
import {Observable} from 'rxjs/Observable';

Injectable()
export class CustomerEffects extends CrudEffect<CustomerEntity> {
  protected payloadType: string = CustomerEntity.type;

  /*
   * create
   * */
  @Effect()
  createDictionary$: Observable<CrudAction<CustomerEntity>> = this.create();

  /*
   * read
   * */
  @Effect()
  readDictionary$: Observable<CrudAction<CustomerEntity>> = this.read();

  /*
   * update
   * */
  @Effect()
  updateDictionary$: Observable<CrudAction<CustomerEntity>> = this.update();

  /*
   * dispose
   * */
  @Effect()
  disposeDictionary$: Observable<CrudAction<CustomerEntity>> = this.dispose();
  constructor(@Inject(Actions) actions$: Actions,
                @Inject(CrudActionFactory) actionFactory: CrudActionFactory,
                @Inject(CrudService) service: CrudService) {
    super(actions$, actionFactory, service);
    }
}
