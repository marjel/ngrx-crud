import {CustomerEntity} from '../model/customer.entity';
import {CrudReducer} from '../../../ngrx-crud/rx/crud.reducer';
import {CustomerState} from '../model/customer.state';
import {CrudState} from '../../../ngrx-crud/model/crud.state';
import {CrudAction} from '../../../ngrx-crud/rx/crud.action';

export class CustomerReducer extends CrudReducer<CustomerEntity> {

  static Reducer(): Function {
    return CustomerReducer.prototype.reducer;
  }

  reducer(state: CustomerState = CustomerState.getDefault(),
          action: CrudAction<CustomerEntity>): CrudState<CustomerEntity> {
    return super.reducer(state, action);
  }
}
