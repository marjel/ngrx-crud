import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from '@angular/common/http';
import {StoreModule} from '@ngrx/store';
import {EffectsModule} from '@ngrx/effects';
import { AppComponent } from './app.component';
import {AppRoutingModule} from './app.routing';
import {FormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {CrudModule} from './ngrx-crud/crud.module';
import {ParametrizationModule} from './ui/parametrization/parametrization.module';
import {BookModule} from './rx/book/book.module';
import { HomeComponent } from './ui/home/home/home.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent
  ],
  imports: [
    CrudModule,
    CommonModule,
    FormsModule,
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    BookModule,
    ParametrizationModule,
    StoreModule.forRoot({}),
    EffectsModule.forRoot([])
  ],
  providers: [
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor() {
  }
}
