import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CrudConfig} from './crud-config';
import {CrudActionFactory} from './rx/crud-action-factory';
import {CrudService} from './persistence/crud.service';
import {HttpClientModule} from '@angular/common/http';

@NgModule({
  imports: [
    CommonModule,
    HttpClientModule
  ],
  providers: [
    CrudConfig,
    CrudActionFactory,
    CrudService
  ]
})
export class CrudModule {

}
