export interface Entity {
  id: string;
  dateCreate: Date;
  dateUpdate: Date;
  getType(): string;
}
