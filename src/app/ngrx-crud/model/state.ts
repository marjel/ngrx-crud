import {CrudEntity} from './crud.entity';
export interface State<T extends CrudEntity  = any> {
  current: T;
  data: Array<T>;
  length: number;
}
