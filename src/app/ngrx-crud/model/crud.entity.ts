import {Entity} from './entity';

export abstract class CrudEntity implements Entity {

  private _id: string;
  private _dateCreate: Date;
  private _dateUpdate: Date;
  private _userCreate: string;
  private _userUpdate: string;

  abstract getType(): string;

  public get id(): string {
    return this._id;
  }

  public set id(value: string) {
    this._id = value;
  }

  public get dateCreate(): Date {
    return this._dateCreate;
  }

  public set dateCreate(value: Date) {
    this._dateCreate = value;
  }

  public get dateUpdate(): Date {
    return this._dateUpdate;
  }

  public set dateUpdate(value: Date) {
    this._dateUpdate = value;
  }

  public get userCreate(): string {
    return this._userCreate;
  }

  public set userCreate(value: string) {
    this._userCreate = value;
  }

  public get userUpdate(): string {
    return this._userUpdate;
  }

  public set userUpdate(value: string) {
    this._userUpdate = value;
  }


  constructor(id: string, dateCreate: Date, dateUpdate: Date, userCreate: string, userUpdate: string) {
    this._id = id;
    this._dateCreate = dateCreate;
    this._dateUpdate = dateUpdate;
    this._userCreate = userCreate;
    this._userUpdate = userUpdate;
  }
}
