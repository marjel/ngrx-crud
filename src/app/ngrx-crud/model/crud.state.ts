import {CrudEntity} from './crud.entity';
import {State} from './state';

export class CrudState<T extends CrudEntity> implements State<T> {

  public data: Array<T>;
  public current: T;



  get length(): number {
    return this.data.length;
  }

 /* static getDefault<E extends CrudEntity  = any>(): CrudState<E> {
    const state: CrudState<E> = new CrudState<E>();
    state.data = new Array<E>();
    state.current = null;
    return state;
  }*/
}
