export interface Crud {
  create(type: any, args?: any);
  read(type: any, args?: any);
  update(type: any, args?: any);
  dispose(type: any, args?: any);
}
