import {CrudEntity} from '../model/crud.entity';

export interface TypedCrud {
  create<T extends CrudEntity>(type: string, entity: T);
  read(type: any, args?: any);
  update<T extends CrudEntity>(type: string, entity: T);
  dispose<T extends CrudEntity>(type: string, entity: T);
}
