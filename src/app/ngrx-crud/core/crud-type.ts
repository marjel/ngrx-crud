export enum CrudType {
  create = 'create', read = 'read', update = 'update', dispose = 'dispose'
}
