import {Observable} from 'rxjs/Observable';
import {Soap} from './remote/soap';
import {Rest} from './remote/rest';
import {Inject, Injectable, Injector} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {CrudResource} from './crud-resource';
import {CrudConfig} from '../crud-config';
import {CrudEntity} from '../model/crud.entity';
import {NodeJson} from './remote/node-json';

Injectable();
export class CrudService<T extends CrudEntity = any> implements CrudResource<T> {

  private crudService: CrudResource<T>;
  private config: CrudConfig;
  private _location: string;

  set location(location: string) {
    this._location = location;
    this.crudService.location = this.getLocation(this.location);
  }
  get location(): string {
    return this._location;
  }

  private getLocation(location: string): string {
    return  location !== null ? `${this.config.resourceLocation}/${location}` : null;
  }
  private getService(injector: Injector): CrudResource<T> {
    switch (this.config.resourceType) {
      case 'soap':
        return new Soap<T>(null);
      case 'json':
        return new NodeJson<T>(injector.get(HttpClient));
    }
    return new Rest<T>(injector.get(HttpClient));
  }

  getAll(location: string = null): Observable<Array<T>> {
    return this.crudService.getAll(this.getLocation(location));
  }

  getById(id: string, location: string = null): Observable<T> {
    return this.crudService.getById(id, this.getLocation(location));
  }

  getRange(start: number, amount: number, location: string = null): Observable<Array<T>> {
    return this.crudService.getRange(start, amount, this.getLocation(location));
  }

  create(item: T, location: string = null): Observable<T> {
    return this.crudService.create(item, this.getLocation(location));
  }

  update(item: T, location: string = null): Observable<T> {
    return this.crudService.update(item, this.getLocation(location));
  }

  deleteById(id: string, location: string = null): Observable<T> {
    return this.crudService.deleteById(id, this.getLocation(location));
  }
  count(location: string = null): Observable<number> {
    return this.crudService.count(this.getLocation(location));
  }

  constructor(@Inject(Injector) injector: Injector,
              @Inject(CrudConfig) config: CrudConfig) {
    this.config = config;
    this.crudService = this.getService(injector);
  }

}
