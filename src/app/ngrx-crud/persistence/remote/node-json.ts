import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {CrudResource} from '../crud-resource';
import {CrudEntity} from '../../model/crud.entity';

export class NodeJson<T extends CrudEntity = any> implements CrudResource<T> {

  private _location: string;

  private headers: HttpHeaders = new HttpHeaders(
    {
      'Content-Type': 'application/json',
      'Cache-control': 'no-cache',
      'Pragma': 'no-cache'
    });
  private options = { headers: this.headers, params: null };
  set location(location: string) {
    this._location = location;
  }
  get location(): string {
    return encodeURI(this._location);
  }
  public getAll(location: string = null): Observable<Array<T>> {
    return this.httpClient
      .get<Array<T>>(location || this.location);
  }
  public getById(id: string, location: string = null): Observable<T> {
    return this.httpClient
      .get<T>(`${(location || this.location)}/${id}`);
  }
  public create(item: T, location: string = null): Observable<T> {
    return this.httpClient
      .post<T>(location || this.location, item, this.options);
  }
  public update(item: T, location: string = null): Observable<T> {
    return this.httpClient
      .put<T>(`${(location || this.location)}/${item.id}`, item, this.options);
  }
  public deleteById(id: string, location: string = null): Observable<T> {
    return this.httpClient
      .delete<T>(`${(location || this.location)}/${id}`, this.options);
  }
  public getRange(fromPage: number, amount: number, location: string = null): Observable<Array<T>> {
    throw new Error('method is not implemented');
  }
  public count(location: string = null): Observable<number> {
    throw new Error('method is not implemented');
  }
  constructor(private httpClient: HttpClient) {
  }
}
