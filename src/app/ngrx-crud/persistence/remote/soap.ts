import {Observable} from 'rxjs/Observable';
import {CrudResource} from '../crud-resource';

export class Soap<T> implements CrudResource<T> {

  constructor(private soapClient: any) {

  }
  set location(path: string) {
  }
  get location(): string {
    return '';
  }
  public getAll(): Observable<Array<T>> {
    throw new Error('method is not implemented');
  }
  getById(id: string): Observable<T> {
    throw new Error('method is not implemented');
  }
  public getRange(start: number, amount: number): Observable<Array<T>> {
    throw new Error('method is not implemented');
  }
  public create(item: T): Observable<T> {
    throw new Error('method is not implemented');
  }
  public update(item: T): Observable<T> {
    throw new Error('method is not implemented');
  }
  public deleteById(id: string): Observable<T> {
    throw new Error('method is not implemented');
  }
  count(): Observable<number> {
    throw new Error('method is not implemented');
  }
}
