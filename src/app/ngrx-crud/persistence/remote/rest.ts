import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs/Observable';
import {CrudResource} from '../crud-resource';
import {CrudEntity} from '../../model/crud.entity';

export class Rest<T extends CrudEntity = any> implements CrudResource<T> {

  private _location: string;

  private headers: HttpHeaders = new HttpHeaders(
    {
      'Content-Type': 'application/json',
      'Cache-control': 'no-cache',
      'Pragma': 'no-cache'
    });
  private options = { headers: this.headers, params: null };
  set location(location: string) {
    this._location = location;
  }
  get location(): string {
    return encodeURI(this._location);
  }
  public getAll(location: string = null): Observable<Array<T>> {
    return this.httpClient
      .get<Array<T>>(location || this.location,  this.getHttpOptions());
  }
  public getById(id: string, location: string = null): Observable<T> {
    return this.httpClient
      .get<T>(location || this.location, this.getHttpOptions([{key: 'id', value: id}]));
  }
  public getRange(fromPage: number, amount: number, location: string = null): Observable<Array<T>> {
    return this.httpClient
      .get<Array<T>>(location || this.location, this.getHttpOptions([
        {key: 'page', value: fromPage},
        {key: 'length', value: amount}
        ]));
  }
  public create(item: T, location: string = null): Observable<T> {
    return this.httpClient
      .post<T>(location || this.location, item, this.getHttpOptions());
  }
  public update(item: T, location: string = null): Observable<T> {
    return this.httpClient
      .put<T>(location || this.location, item, this.getHttpOptions());
  }
  public deleteById(id: string, location: string = null): Observable<T> {
    return this.httpClient
      .delete<T>(location || this.location, this.getHttpOptions([{key: 'id', value: id}]));
  }
  public count(location: string = null): Observable<number> {
    return this.httpClient
      .get<number>((location || this.location) + '/count');
  }

  private getHttpOptions(params: any[] = null) {
    this.options.params = this.getHttpParams(params);
    return this.options;
  }

  private getHttpParams(params: any[] = null): HttpParams {
    let httpParams: HttpParams = new HttpParams();
    if (params !== null && params.length) {
      params
      .filter(item => (item.value !== undefined && item.value !== null && item.value !== ''))
      .forEach(item => {
        httpParams = httpParams.append(item.key, item.value);
      });
    }
    return httpParams;
  }

  constructor(private httpClient: HttpClient) {
  }
}
