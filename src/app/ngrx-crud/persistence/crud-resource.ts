import {Observable} from 'rxjs/Observable';

export interface CrudResource<T = any> {
  location?: string;
  getAll(a?: any): Observable<Array<T>>;
  getRange?(startPage: number, amount: number, a?: any): Observable<Array<T>>;
  getById(id: string, a?: any): Observable<T>;
  create(item: T, a?: any): Observable<T>;
  update(item: T, a?: any): Observable<T>;
  deleteById(id: string, a?: any): Observable<T>;
  count?(a?: any): Observable<number>;
}
