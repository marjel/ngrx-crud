import {environment} from '../../environments/environment';
import {Injectable} from '@angular/core';

@Injectable()
export class CrudConfig {

  private _paylaodType: string;
  private _resourceLocation: string;
  private _resourceType: string;

  public get paylaodType(): string {
    return this._paylaodType;
  }

  public set paylaodType(value: string) {
    this._paylaodType = value;
  }

  public get resourceLocation(): string {
    return this._resourceLocation;
  }

  public set resourceLocation(value: string) {
    this._resourceLocation = value;
  }

  public get resourceType(): string {
    return this._resourceType;
  }

  public set resourceType(value: string) {
    this._resourceType = value;
  }

  constructor() {
    this._resourceLocation = environment.remotePath;
    this._resourceType = environment.remoteType;
  }
}
