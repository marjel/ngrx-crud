import {Context} from './context';
import {CrudEntity} from '../model/crud.entity';
import {Store} from '@ngrx/store';
import {TypedCrud} from '../core/typed-crud';

export interface Mediator<E extends CrudEntity = any> extends TypedCrud {
  addContext(context: Context<E>);
  removeContext(name?: string);
  getContext(name?: string): Context<E>;
  subscribe(subscriber);
  dispatch<T extends CrudEntity>(type: string, payload: T);
  getAll<T extends CrudEntity>(type: string): Store<T[]>;
  getSelected<T extends CrudEntity>(type: string): Store<T>;
  getById<T extends CrudEntity>(type: string, id: string): Store<T>;
}
