import {Field} from './field';
import {InputType} from './input-type';
import {isBoolean, isString, isUndefined} from 'util';
import {isDefined} from '@angular/compiler/src/util';

export class BaseField implements Field {

  name: string;
  label: string;
  required: boolean;
  pattern: string;
  type: InputType;

  constructor(name: string, label: string, required?: boolean, pattern?: string, type?: InputType) {
    this.name = name;
    this.label = label;
    this.required = isBoolean(required) ? required : true;
    this.pattern = isString(pattern) ? pattern : '[A-Za-z0-9\\ \\_\\-*ęóąśłżźćńĘÓĄŚŁŻŹĆŃ]+';
    this.type = isDefined(type) ? type : InputType.text;
  }
}
