import {InputType} from './input-type';

export interface Field {
  name: string;
  label: string;
  required: boolean;
  type: InputType;
  description?: string;
  min?: number;
  max?: number;
  pattern?: string;
}
