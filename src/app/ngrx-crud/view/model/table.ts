import {Field} from './field';

export interface Table {
  columns: Field[];
  length?: number;
  lengthOptions?: number[];
}
