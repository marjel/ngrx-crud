import {Context} from './context';

export abstract class CrudComponent<T, C extends Context<T>> {

  protected context: C;

  constructor(context: C) {
    this.context = context;
  }
}
