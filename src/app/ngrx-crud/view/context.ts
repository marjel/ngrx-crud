import {Observable} from 'rxjs/Observable';
import {Subscription} from 'rxjs/Subscription';
import {Action} from '@ngrx/store';

export interface Context<T> {
  name: string;
  data$?: Observable<any[]>;
  closed?: boolean;
  subscribe(next?: (value: T) => void, error?: (error: any) => void, complete?: () => void): Subscription;
  dispatch<V extends Action = Action>(action: V): void;

}
