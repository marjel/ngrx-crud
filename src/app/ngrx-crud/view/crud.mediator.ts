import {Mediator} from './mediator';
import {Context} from './context';
import {CrudEntity} from '../model/crud.entity';
import {CrudActionFactory} from '../rx/crud-action-factory';
import {createFeatureSelector, createSelector, MemoizedSelector, Store} from '@ngrx/store';
import {CrudStatus} from '../core/crud-status';
import {CrudState} from '../model/crud.state';
import {CrudType} from '../core/crud-type';
import {CustomerEntity} from '../../rx/customer/model/customer.entity';
import {BookEntity} from '../../rx/book/model/book.entity';
import {DictionaryEntity} from '../../rx/dictionary/model/dictionary.entity';

export abstract class CrudMediator<E extends CrudEntity = any> implements Mediator<E> {

  protected context: Context<E>;
  protected contextList: Array<Context<E>> = new Array<Context<E>>();

  addContext(context: Context<E>) {
    this.contextList.push(context);
  }
  removeContext() {
    this.context = null;
  }
  getContext(): Context<E> {
    return this.context;
  }

  subscribe(subscriber = null) {
    this.context.subscribe(subscriber || this.subscriber.bind(this));
  }

  protected subscriber<T extends CrudEntity>(currentChangeObject: any) {
    switch (currentChangeObject.type) {
      case CrudType.create:
        this.create<T>(currentChangeObject.payloadType, currentChangeObject.payload);
        break;
      case CrudType.read:
        this.context.data$ = this.getAll<T>(currentChangeObject.payloadType);
        this.read(currentChangeObject.payloadType);
        break;
      case CrudType.update:
        this.update<T>(currentChangeObject.payloadType, currentChangeObject.payload);
        break;
      case CrudType.dispose:
        this.update<T>(currentChangeObject.payloadType, currentChangeObject.payload);
        break;
    }
  }

  dispatch<T extends CrudEntity>(type: string, payload: any) {
    const t: any = payload.hasOwnProperty('id') ? payload.getType() : payload;
    this.store.dispatch(this.factory.getAction(type, CrudStatus.pending, t, payload));
  }

  protected getFeatureSelector<T extends CrudEntity>(type: string): MemoizedSelector<Object, CrudState<T>> {
    return createFeatureSelector<CrudState<T>>(type);
  }

  protected getAllDataSelector<T extends CrudEntity>(type: string): MemoizedSelector<Object, Array<T>> {
    return createSelector(this.getFeatureSelector<T>(type), (state: CrudState<T>) => state.data);
  }

  protected getEntitySelector<T extends CrudEntity>(type: string): MemoizedSelector<Object, T> {
    return createSelector(this.getFeatureSelector<T>(type), (state: CrudState<T>) => state.current);
  }

  getAll<T extends CrudEntity>(type: string): Store<T[]> {
    return this.store.select(this.getAllDataSelector<T>(type));
  }

  getSelected<T extends CrudEntity>(type: string): Store<T> {
    return this.store.select(this.getEntitySelector<T>(type));
  }

  getById<T extends CrudEntity>(type: string, id: string): Store<T> {
    throw new Error('method is not implemented');
  }
  /*
 * read
 * */
  read(type: any, args?: any) {
    this.dispatch(CrudType.read, type);
  }
  /*
* create
* */
  create<T>(type: string, entity: T) {
    this.dispatch(CrudType.create, entity);
  }
  /*
* update
* */
  update<T>(type: string, entity: T) {
    this.dispatch(CrudType.create, entity);
  }
  /*
* dispose
* */
  dispose<T>(type: string, entity: T) {
    this.dispatch(CrudType.create, entity);
  }
  constructor(context: Context<E>,
              protected store: Store<any>,
              protected factory: CrudActionFactory) {
    this.context = context;
    this.subscribe();
  }
}
