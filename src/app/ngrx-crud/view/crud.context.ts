import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Context} from './context';
import {Observable} from 'rxjs/Observable';
import {Crud} from '../core/crud';
import {CrudType} from '../core/crud-type';

export abstract class CrudContext<T> extends BehaviorSubject<any> implements Context<T>, Crud {

  name = 'root'; // the name for a main context, for children contexts it have to be override
  abstract data$: Observable<Array<T>>;
  abstract entity$: Observable<T>;
  initialized = 0;
  dispatch(value: any): void {
    this.next(value);
  }
  /*
 * create
 * */
  create(type: any, args?: any) {
    this.dispatch({type: CrudType.create, payloadType: type, payload: args});
  }
  /*
 * read
 * */
  read(type: any, args?: any) {
    this.dispatch({type: CrudType.read, payloadType: type, payload: null});
  }
  /*
 * update
 * */
  update(type: any, args?: any) {
    this.dispatch({type: CrudType.update, payloadType: type, payload: args});
  }
  /*
 * dispose
 * */
  dispose(type: any, args?: any) {
    this.dispatch({type: CrudType.dispose, payloadType: type, payload: args});
  }

  constructor() {
    super({});
  }
}
