import {CrudState} from '../model/crud.state';
import {CrudAction} from './crud.action';
import {CrudEntity} from '../model/crud.entity';

export interface Reducer<E extends CrudEntity> {
  reducer(state: CrudState<E>, action: CrudAction<E>): CrudState<E>;
}
