import {CrudAction} from './crud.action';
import {Inject, Injectable} from '@angular/core';
import {CrudConfig} from '../crud-config';

Injectable();
export class CrudActionFactory {
  payloadType: string;


  getType(payloadType: string, type: string, status: string): string {
    return `${payloadType}.${type}.${status}`;
  }

  getAction<T>(type: string, status: string, payloadType: string, payload: any = null): CrudAction<T> {
    this.payloadType = payloadType;
    return new CrudAction<T>(type, status, payloadType, payload);
  }

  constructor(@Inject(CrudConfig) private crudConfig: CrudConfig) {

  }
}
