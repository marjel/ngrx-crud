import { Observable } from 'rxjs/Observable';
import { Effect, Actions } from '@ngrx/effects';

import {CrudActionFactory} from './crud-action-factory';
import {CrudType} from '../core/crud-type';
import {CrudStatus} from '../core/crud-status';
import {CrudAction} from './crud.action';
import {CrudService} from '../persistence/crud.service';
import {CrudEntity} from '../model/crud.entity';

export abstract class CrudEffect<E  extends CrudEntity = any> {

  protected abstract payloadType: string;
  /*
   * create
   * */
  protected create(): Observable<CrudAction<E>> {
    return this.actions$
      .ofType(this.actionFactory.getType(this.payloadType, CrudType.create, CrudStatus.pending))
      .switchMap((a: CrudAction<E>) => {
          return this.service.create(a.payload, this.payloadType)
            .map((d: E[]) => this.actionFactory.getAction<E>(CrudType.create, CrudStatus.success, a.payloadType, d))
            .catch(() => Observable.of(
              this.actionFactory.getAction<E>(CrudType.create, CrudStatus.fail, a.payloadType, null)
            ));
      });
  }
  /*
 * read
 * */
  protected read(): Observable<CrudAction<E>> {
    return this.actions$
      .ofType<CrudAction<E>>(this.actionFactory.getType(this.payloadType, CrudType.read, CrudStatus.pending))
      .mergeMap((a: CrudAction<E>) => {
        return this.service.getAll(this.payloadType)
          .map((d: E[]) => this.actionFactory.getAction<E>(CrudType.read, CrudStatus.success, a.payloadType, d))
          .catch(() => Observable.of(
            this.actionFactory.getAction<E>(CrudType.read, CrudStatus.fail, a.payloadType, null)
          ));
      });
  }
  /*
 * update
 * */
  protected update(): Observable<CrudAction<E>> {
    return this.actions$
      .ofType<CrudAction<E>>(this.actionFactory.getType(this.payloadType, CrudType.update, CrudStatus.pending))
      .switchMap((a: CrudAction<E>) => {
        return this.service.update(a.payload, this.payloadType)
          .map((d: E[]) => this.actionFactory.getAction<E>(CrudType.update, CrudStatus.success, a.payloadType, d))
          .catch(() => Observable.of(
            this.actionFactory.getAction<E>(CrudType.update, CrudStatus.fail, a.payloadType, null)
          ));
      });
  }
  /*
 * dispose
 * */
  protected dispose(): Observable<CrudAction<E>> {
    return this.actions$
      .ofType<CrudAction<E>>(this.actionFactory.getType(this.payloadType, CrudType.dispose, CrudStatus.pending))
      .switchMap((a: CrudAction<E>) => {
        return this.service.deleteById(a.payload[0].id, this.payloadType)
          .map(() => this.actionFactory.getAction<E>(CrudType.dispose, CrudStatus.success, a.payloadType, a.payload[0].id))
          .catch(() => Observable.of(
            this.actionFactory.getAction<E>(CrudType.dispose, CrudStatus.fail, a.payloadType, null)
          ));
      });
  }
  constructor(protected actions$: Actions,
              protected actionFactory: CrudActionFactory,
              protected service: CrudService) {
  }
}
