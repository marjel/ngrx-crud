import {Action} from '@ngrx/store';

export class CrudAction<T> implements Action {

  private _type: string;
  private _status: string;
  private _payloadType: string;
  private _payload: Array<T>;

  public get payload(): Array<T> {
    return this._payload;
  }
  public get payloadType(): string {
    return this._payloadType;
  }
  public get type(): string {
     return `${this._payloadType}.${this._type}.${this._status}`;
  }
  public get status(): string {
    return this._status;
  }
  public get shortType(): string {
    return this._type;
  }

  constructor(type: string, status: string, payloadType: string, payload: Array<T>) {
    this._type = type;
    this._status = status;
    this._payloadType = payloadType;
    this._payload = payload;
  }
}
