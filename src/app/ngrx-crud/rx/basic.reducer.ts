import {Reducer} from './reducer';
import {CrudState} from '../model/crud.state';
import {CrudAction} from './crud.action';
import {CrudEntity} from '../model/crud.entity';

export abstract class BasicReducer<E  extends CrudEntity = any> implements Reducer<E> {

  abstract reducer(state: CrudState<E>, action: CrudAction<E>): CrudState<E>;

}
