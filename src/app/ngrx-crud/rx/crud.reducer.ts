import {CrudState} from '../model/crud.state';
import {CrudAction} from './crud.action';
import {BasicReducer} from './basic.reducer';
import {CrudStatus} from '../core/crud-status';
import {CrudType} from '../core/crud-type';
import {CrudEntity} from '../model/crud.entity';

export abstract class CrudReducer<E  extends CrudEntity = any> extends BasicReducer<E> {
  reducer(state: CrudState<E>, action: CrudAction<E>): CrudState<E> {
    switch (action.shortType) {
      /*
       * create
       * */
      case CrudType.create: {
        const item: E = action.payload[0];
        if (action.status === CrudStatus.success) {
          return <CrudState<E>>{
            ...state,
            data: [...state.data, item]
          };
        }
      // TODO - in fail case
        return null;
      }
      /*
       * read
       * */
      case CrudType.read: {
        const items = action.payload;
        if (action.status === CrudStatus.success) {
          return <CrudState<E>>{
            ...state,
            data: items
          };

        }
        // TODO - in fail case
        return null;
      }
      /*
       * update
       * */
      case CrudType.update: {
        const item = action.payload[0];
        // if (action.status === CrudStatus.success) {
        //   const items = state.data
        //   .map(d => (d[0].id === item.id) ? item : d[0]);
        //   return <CrudState<E>>{
        //     ...state,
        //     data: [...items]
        //   };
        // }
      // TODO - in fail case
        return null;
      }
      /*
       * dispose
       * */
      case CrudType.dispose: {
        const item: E = action.payload[0];
        // if (action.status === CrudStatus.success) {
        //   const id = item.id;
        //   return <CrudState<E>>{
        //     ...state,
        //     data: state.data.filter(d => d[0].id !== id)
        //   };
        // }
        // TODO - in fail case
        return null;
      }
      default: {
        return state;
      }
    }
  }

}
