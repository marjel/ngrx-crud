import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {DictionaryEntity} from '../../rx/dictionary/model/dictionary.entity';

import {BookEntity} from '../../rx/book/model/book.entity';
import {CrudContext} from '../../ngrx-crud/view/crud.context';
import {CustomerEntity} from '../../rx/customer/model/customer.entity';

@Injectable()
export class ParametrizationContext extends CrudContext<DictionaryEntity | CustomerEntity | BookEntity> {

  data$: Observable<Array<DictionaryEntity | CustomerEntity | BookEntity>>;
  entity$: Observable<DictionaryEntity | CustomerEntity | BookEntity>;
  constructor() {
    super();
  }

}
