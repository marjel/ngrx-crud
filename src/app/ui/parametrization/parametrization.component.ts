import { Component, OnInit } from '@angular/core';
import {ParametrizationContext} from './parametrization.context';
import {CrudComponent} from '../../ngrx-crud/view/crud.component';
import {DictionaryEntity} from '../../rx/dictionary/model/dictionary.entity';
import {BookEntity} from '../../rx/book/model/book.entity';
import {CustomerEntity} from '../../rx/customer/model/customer.entity';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-parametrization',
  templateUrl: './parametrization.component.html'
})
export class ParametrizationComponent
  extends CrudComponent<DictionaryEntity | CustomerEntity | BookEntity,
    ParametrizationContext>
  implements OnInit {

  ngOnInit() {
  }
/*  book() {
    this.context.read(BookEntity.type);
  }
  dictionary() {
    this.context.read(DictionaryEntity.type);
  }
  customer() {
    this.context.read(CustomerEntity.type);
  }*/
  constructor(context: ParametrizationContext, public route: ActivatedRoute) {
    super(context);
  }
}
