import {BaseField} from '../../ngrx-crud/view/model/base-field';
import {Table} from '../../ngrx-crud/view/model/table';

const dictionary: Table = {
  length: 10,
  lengthOptions: [
    5, 10, 20, 50
  ],
  columns: [
    new BaseField('tableName', 'DICTIONARY.TABLE'),
    new BaseField('fieldName', 'DICTIONARY.FIELD'),
    new BaseField('value1', 'DICTIONARY.VALUE1'),
    new BaseField('value2', 'DICTIONARY.VALUE2'),
    new BaseField('deposit', 'DICTIONARY.DEPOSIT'),
    new BaseField('description', 'DICTIONARY.DESCRIPTION'),
    new BaseField('market', 'DICTIONARY.MARKET')
  ]
};

export const dataTableConfig = [
  dictionary
];
