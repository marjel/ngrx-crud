import {Injectable} from '@angular/core';
import {CrudMediator} from '../../ngrx-crud/view/crud.mediator';
import {ParametrizationContext} from './parametrization.context';
import {CrudActionFactory} from '../../ngrx-crud/rx/crud-action-factory';
import {CrudType} from '../../ngrx-crud/core/crud-type';
import {CrudStatus} from '../../ngrx-crud/core/crud-status';
import {Store} from '@ngrx/store';
import {DictionaryEntity} from '../../rx/dictionary/model/dictionary.entity';
import {BookEntity} from '../../rx/book/model/book.entity';
import {CustomerEntity} from '../../rx/customer/model/customer.entity';

@Injectable()
export class ParametrizationMediator
  extends CrudMediator<DictionaryEntity | CustomerEntity | BookEntity> {
  constructor(context: ParametrizationContext,
              store: Store<any>,
              factory: CrudActionFactory) {
    super(context, store, factory);
  }
}
