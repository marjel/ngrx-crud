import {NgModule} from '@angular/core';
import {ParametrizationComponent} from './parametrization.component';
import {ParametrizationMediator} from './parametrization.mediator';
import {RouterModule, Routes} from '@angular/router';
import {DictionaryModule} from '../../rx/dictionary/dictionary.module';
import {ParametrizationContext} from './parametrization.context';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {BookModule} from '../../rx/book/book.module';
import {CustomerModule} from '../../rx/customer/customer.module';
import {
  StoreRouterConnectingModule,
  RouterStateSerializer
} from '@ngrx/router-store';
import {EntitySerializer} from './router/entitiy-serializer';

export const routes: Routes = [
  {
    path: 'parametrization/:entity',
    component: ParametrizationComponent
  }
];

@NgModule({
  declarations: [
    ParametrizationComponent
  ],
  providers: [
    {
      provide: RouterStateSerializer,
      useClass: EntitySerializer
    },
    EntitySerializer,
    ParametrizationMediator,
    ParametrizationContext
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    DictionaryModule, /* entity type 1 */
    CustomerModule, /* entity type 2 */
    BookModule, /* entity type 3 */
    RouterModule.forRoot(routes),
    StoreRouterConnectingModule
  ],
  exports: [
    RouterModule
  ]
})
export class ParametrizationModule {
  constructor(m: ParametrizationMediator) {
  }
}
