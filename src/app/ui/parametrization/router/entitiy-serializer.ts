import {RouterStateSnapshot} from '@angular/router';
import {RouterStateSerializer} from '@ngrx/router-store';
import {RouterState} from './route-rx';
import {CrudType} from '../../../ngrx-crud/core/crud-type';
import {CrudStatus} from '../../../ngrx-crud/core/crud-status';
import {Inject, Injectable} from '@angular/core';
import {Store} from '@ngrx/store';
import {CrudActionFactory} from '../../../ngrx-crud/rx/crud-action-factory';
import {isString} from 'util';

Injectable();
export class EntitySerializer implements RouterStateSerializer<RouterState> {


  constructor(
    @Inject(Store) private store: Store<any>,
              @Inject(CrudActionFactory) private actionFactory: CrudActionFactory
  ) {
  }

  serialize(routerState: RouterStateSnapshot): RouterState {

    let route = routerState.root;

    while (route.firstChild) {
      route = route.firstChild;
    }

    const { url } = routerState;
    const queryParams = routerState.root.queryParams;
    const params = route.params;
    if (isString(params.entity)) {
      this.store.dispatch(
        this.actionFactory.getAction(CrudType.read, CrudStatus.pending, params.entity)
      );
    } else if (isString(params.resolve)) {

    }

    return { url, params, queryParams };
  }
}
