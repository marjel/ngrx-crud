import {routerReducer, RouterReducerState} from '@ngrx/router-store';
import {ActionReducerMap} from '@ngrx/store';
import {Params} from '@angular/router';

export interface RouterState {
  url: string;
  params: Params;
  queryParams: Params;
}

export interface State {
  routerReducer: RouterReducerState<RouterState>;
}


export const reducers: ActionReducerMap<State> = {
  routerReducer: routerReducer
};
