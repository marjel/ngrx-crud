# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

## Project structure ##
app
	
	-- ngrx-crud
		*crud.module.ts
		*crud-config.ts
			-- core
					*typed-crud.ts
					*crud-status.ts
					*crud-type.ts
			-- model
					*entity.ts
					*state.ts
					*crud.entity.ts
					*crud.state.ts
			-- persistence
					*rest.ts
					*soap.ts
					*crud-resource.ts
					*crud.service.ts
			-- rx
					*reducer.ts
					*basic.reducer.ts
					*crud.reducer.ts
					*crud.effect.ts
					*crud.action.ts
					*crud-action-factory.ts
			-- view
					-- model
							*field.ts
							*table.ts
							*input-type.ts
							*basic.field.ts -> change name to base.field
					*context.ts
					*mediator.ts
					*crud.mediator.ts
					*crud.component.ts
	
	
	-- rx
			*rxjs-operators.ts
			-- feature
					*feature.module.ts
					-- model
							*feature.entity.ts
							*feature.state.ts
					-- store
							*feature.effect.ts
							*feature.reducer.ts
					
			
	-- ui
			*app.module.ts
			*app.component.ts
			*app.mediator.ts
			*app.context.ts
			-- common
			-- feature
					*feature.component.ts
					*feature.context.ts
					*feature.mediator.ts
					*feature.module.ts
					

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
