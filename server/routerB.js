'use strict';

const express = require('express');
const low = require('lowdb');
const path = require('path');
const storage = require('lowdb/adapters/FileSync');
const uuid = require('node-uuid');

const bookAdapter = new storage(path.join(__dirname, 'book.json'));
const bookDB = low(bookAdapter);

const dictionaryAdapter = new storage(path.join(__dirname, 'dictionary.json'));
const dictionaryDB = low(dictionaryAdapter);

const router = new express.Router();
module.exports = router;

router.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET, DELETE, OPTIONS, POST, PUT');
  res.header('Access-Control-Allow-Headers', 'Accept, Content-Type, Origin, X-Requested-With');
  next();
});

/*
*  dictionary
* */
router.post('/dictionary', (req, res) => {
  let data = req.body;
  data.id = uuid.v4();
  let d = dictionaryDB.get('dictionary').push(data).last().value();
  res.status(200).json(d);
});

router.get('/dictionary', (req, res) => {
  res.status(200).json(dictionaryDB.get('dictionary').value());
});

router.get('/dictionary/:id', (req, res) => {
  let id = req.params.id;
  let d = dictionaryDB.get('dictionary').find({id}).value();
  res.status(200).json(d);
});

router.put('/dictionary/:id', (req, res) => {
  let id = req.params.id;
  let d = dictionaryDB.get('dictionary').find({id}).assign(req.body).value();
  res.status(200).json(d);
});

router.delete('/dictionary/:id', (req, res) => {
  let id = req.params.id;
  let d = dictionaryDB.get('dictionary').find({id}).value();
  dictionaryDB.get('dictionary').remove({id}).value();
  res.status(200).json(d);
});

/*
*  book
* */
router.post('/book', (req, res) => {
  let data = req.body;
  data.id = uuid.v4();
  let b = bookDB.get('book').push(data).last().value();
  res.status(200).json(b);
});

router.get('/book', (req, res) => {
  res.status(200).json(bookDB.get('book').value());
});

router.get('/book/:id', (req, res) => {
  let id = req.params.id;
  let b = bookDB.get('book').find({id}).value();
  res.status(200).json(b);
});

router.put('/book/:id', (req, res) => {
  let id = req.params.id;
  let b = bookDB.get('book').find({id}).assign(req.body).value();
  res.status(200).json(b);
});

router.delete('/book/:id', (req, res) => {
  let id = req.params.id;
  let b = bookDB.get('book').find({id}).value();
  bookDB.get('book').remove({id}).value();
  res.status(200).json(b);
});
