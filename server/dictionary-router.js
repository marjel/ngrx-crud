'use strict';

const router = require('./router');
const uuid = require('node-uuid');
const low = require('lowdb');
const path = require('path');
const storage = require('lowdb/adapters/FileSync');

const adapter = new storage(path.join(__dirname, 'dictionary.json'));
const db = low(adapter);

router.post('/dictionary', (req, res) => {
  let data = req.body;
  data.id = uuid.v4();
  let d = db.get('dictionary').push(data).last().value();
  res.status(200).json(d);
});

router.get('/dictionary', (req, res) => {
  res.status(200).json(db.get('dictionary').value());
});

router.get('/dictionary/:id', (req, res) => {
  let id = req.params.id;
  let d = db.get('dictionary').find({id}).value();
  res.status(200).json(d);
});

router.put('/dictionary/:id', (req, res) => {
  let id = req.params.id;
  let d = db.get('dictionary').find({id}).assign(req.body).value();
  res.status(200).json(d);
});

router.delete('/dictionary/:id', (req, res) => {
  let id = req.params.id;
  let d = db.get('dictionary').find({id}).value();
  db.get('dictionary').remove({id}).value();
  res.status(200).json(d);
});

module.exports = router;
