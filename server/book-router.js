'use strict';

const router = require('./router');
const uuid = require('node-uuid');
const low = require('lowdb');
const path = require('path');
const storage = require('lowdb/adapters/FileSync');

const adapter = new storage(path.join(__dirname, 'book.json'));
const db = low(adapter);

router.post('/book', (req, res) => {
  let data = req.body;
  data.id = uuid.v4();
  let b = db.get('book').push(data).last().value();
  res.status(200).json(b);
});

router.get('/book', (req, res) => {
  res.status(200).json(db.get('book').value());
});

router.get('/book/:id', (req, res) => {
  let id = req.params.id;
  let b = db.get('book').find({id}).value();
  res.status(200).json(b);
});

router.put('/book/:id', (req, res) => {
  let id = req.params.id;
  let b = db.get('book').find({id}).assign(req.body).value();
  res.status(200).json(b);
});

router.delete('/book/:id', (req, res) => {
  let id = req.params.id;
  let b = db.get('book').find({id}).value();
  db.get('book').remove({id}).value();
  res.status(200).json(b);
});

module.exports = router;
